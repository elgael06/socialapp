import React from 'react';
import {
  SafeAreaView,
  ScrollView,
  StatusBar,
  Text,
  useColorScheme,
} from 'react-native';
import * as firebase from 'firebase';
import {Colors} from 'react-native/Libraries/NewAppScreen';

const config = {
  // apiKey: "AIzaSyC-3QDYXdZgd6qaRUIIVtgMw8me27JiofE",
  // authDomain: "thinkapp-88ee2.firebaseapp.com",
  // databaseURL: "https://thinkapp-88ee2.firebaseio.com",
  // projectId: "thinkapp-88ee2",
  // storageBucket: "thinkapp-88ee2.appspot.com",
  // messagingSenderId: "734392291102",
  // appId: "1:734392291102:web:b7a2653c3e3b428c74830f",
  // measurementId: "G-WGMCWHV0M1"
  apiKey: 'AIzaSyBaU5VlOLb9IWlKYczOm5wOQKyuuIMmYjQ',
  authDomain: '',
  databaseURL: 'https://consultathinkfirebase.firebaseio.com',
  storageBucket: 'consultathinkfirebase.appspot.com',
};

firebase.initializeApp(config);

const App = () => {
  const isDarkMode = useColorScheme() === 'dark';

  const backgroundStyle = {
    backgroundColor: isDarkMode ? Colors.darker : Colors.lighter,
  };

  return (
    <SafeAreaView style={backgroundStyle}>
      <StatusBar barStyle={isDarkMode ? 'light-content' : 'dark-content'} />
      <ScrollView
        contentInsetAdjustmentBehavior="automatic"
        style={backgroundStyle}>
        <Text>listo</Text>
      </ScrollView>
    </SafeAreaView>
  );
};

export default App;
